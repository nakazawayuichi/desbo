# coding: utf-8
from django.contrib.auth.models import User
from rest_framework import serializers
from api.models import Schedule
import pytz


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('id', 'user', 'is_attend', 'destination', 'attend_datetime', 'sort_order')

class UserScheduleSerializer(serializers.ModelSerializer):
    display_name = serializers.SerializerMethodField()
    schedule_id = serializers.SerializerMethodField()
    is_attend = serializers.SerializerMethodField()
    is_locked  = serializers.SerializerMethodField()
    destination = serializers.SerializerMethodField()
    attend_datetime = serializers.SerializerMethodField()
    attend_date = serializers.SerializerMethodField()
    attend_time = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'display_name', 'schedule_id', 'is_attend', 'destination', 'attend_datetime', 'attend_date', 'attend_time', 'is_locked', )

    def get_display_name(self, user):
        return user.first_name if user.first_name else user.username

    def get_schedule_id(self, user):
        schedule = self.context.get('schedule')
        return schedule.id if schedule  else None

    def get_is_attend(self, user):
        schedule = self.context.get('schedule')
        return schedule.is_attend if schedule  else False

    def get_is_locked(self, user):
        is_locked = self.context.get('is_locked')
        return is_locked

    def get_destination(self, user):
        schedule = self.context.get('schedule')
        return schedule.destination if schedule  else ''

    def get_attend_datetime(self, user):
        schedule = self.context.get('schedule')
        if schedule and schedule.attend_datetime:
            return schedule.attend_datetime.astimezone(pytz.timezone('Asia/Tokyo'))
        else:
            return ''

    def get_attend_date(self, user):
        schedule = self.context.get('schedule')
        if schedule and schedule.attend_datetime:
            return schedule.attend_datetime.astimezone(pytz.timezone('Asia/Tokyo')).strftime('%Y/%m/%d')
        else:
            return ''

    def get_attend_time(self, user):
        schedule = self.context.get('schedule')
        if schedule and schedule.attend_datetime:
            return schedule.attend_datetime.astimezone(pytz.timezone('Asia/Tokyo')).strftime('%H:%M')
        else:
            return ''


class SetScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Schedule
        fields = ('is_attend', 'destination', 'attend_datetime', 'user', )

    def to_internal_value(self, data):
        data = data.copy()
        data['user'] = self.context['user_id']
        return super(SetScheduleSerializer, self).to_internal_value(data)

    def validate(self, data):
        super(SetScheduleSerializer, self).validate(data)

        if self.context['schedule_id']:
            try:
                schedule = Schedule.objects.get(pk=self.context['schedule_id'], user_id=self.context['user_id'])
            except Schedule.DoesNotExist:
                raise serializers.ValidationError("Schedule DoesNotExist")
        return data