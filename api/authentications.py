# coding: utf-8
import os
#from django.core import exceptions
#from django.contrib.admin import exceptions
from rest_framework import authentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import *

class FooAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth_token = request.META.get('HTTP_AUTHORIZATION')
        if not auth_token:
            raise AuthenticationFailed('Authentication token is none')
        try:
            token = Token.objects.get(key=auth_token.replace('Token ', ''))
        except Token.DoesNotExist:
            raise AuthenticationFailed('error')

        return token.user, None
