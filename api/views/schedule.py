# coding: utf-8
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
import django_filters
from rest_framework import viewsets, filters, mixins
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from api.authentications import FooAuthentication
from api.models import Schedule
from api.serializers import ScheduleSerializer, UserScheduleSerializer, SetScheduleSerializer
from desbo import settings


class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all().order_by('user__userprofile__display_order')
    serializer_class = ScheduleSerializer
    authentication_classes = (FooAuthentication,)

class ScheduleView(APIView):
    """
    スケジュール取得
    """

    def get(self, request):
        users = User.objects. \
            select_related('userprofile'). \
            prefetch_related('schedules'). \
            all().order_by('user__userprofile__display_order')

        login_user_id = request.user.id
        is_staff = request.user.is_staff

        users = User.objects. \
            prefetch_related('schedules'). \
            filter(Q(is_staff=False), Q(is_staff=False)).order_by('id')

        user_schedules = []
        for user in users:
            schedule = None
            is_locked = False
            if user.schedules.exists():
                schedule = user.schedules.all()[0]

            if not is_staff and login_user_id != user.id:
                is_locked = True

            serializer = UserScheduleSerializer(user, context={'schedule':schedule, 'is_locked':is_locked})
            user_schedules.append(serializer.data)

        return Response(dict(user_schedules=user_schedules))


user_schedules = ScheduleView.as_view()


class SetScheduleViewSet(viewsets.ModelViewSet):
    """
    スケジュール設定
    """
    serializer_class = SetScheduleSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    queryset = Schedule.objects.all()

    def get_serializer_context(self):
        context = super(SetScheduleViewSet, self).get_serializer_context()
        context['schedule_id'] = self.kwargs.get('pk', None)
        context['user_id'] = self.kwargs['user_id']
        context['user'] = User.objects.get(pk=context['user_id'])
        return context
