from django.contrib import admin

from api.models import UserProfile, Schedule

admin.site.register(UserProfile)


class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('id', 'user',)
    search_fields = ['user',]  # 検索ボックス を出す

admin.site.register(Schedule, admin_class=ScheduleAdmin)