from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import string_concat
from rest_framework.authtoken.models import Token

from desbo import settings


class BaseModel(models.Model):
    """ 基底モデル """
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta(object):
        abstract = True
        app_label = 'apps'


class UserProfile(BaseModel):
    """ ユーザプロファイル """
    ROLE_ADMIN = 1  # 管理者
    ROLE_STANDARD = 2  # 一般ユーザ

    user = models.OneToOneField(User, verbose_name=_('user'))  # ユーザ
    display_order = models.IntegerField(_('display order'), blank=True, null=True, default=0)  # 表示順

    class Meta:
        verbose_name = _('UserProfile')
        verbose_name_plural = string_concat('001. ', verbose_name)

    def __str__(self):
        return 'User profile for %s' % self.user


class Schedule(BaseModel):
    """ スケジュール """
    user = models.ForeignKey(User, verbose_name=_('user'), related_name='schedules')  # ユーザ
    is_attend = models.BooleanField(_('is attend'), default=False)  # 出社
    destination = models.TextField(verbose_name='destination', blank=True)  # 行き先
    attend_datetime = models.DateTimeField(_('attend datetime'), blank=True, null=True)  # 出社日時
    sort_order = models.IntegerField(_('sort order'), blank=True, null=True, default=0)  # 並び順

    class Meta:
        verbose_name = _('Schedule')
        verbose_name_plural = string_concat('002. ', verbose_name)

    def __str__(self):
        return u"Schedule for %s" % self.user


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
