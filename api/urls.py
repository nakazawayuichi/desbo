# coding: utf-8
from django.conf.urls import patterns, url, include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from api.views.schedule import ScheduleViewSet, user_schedules, SetScheduleViewSet
from api.views.user import UserViewSet
from rest_framework_extensions.routers import ExtendedSimpleRouter

router = routers.DefaultRouter()
router.register(r'schedules', ScheduleViewSet)
router.register(r'users', UserViewSet)

user_router = ExtendedSimpleRouter()
user_router.register(r'set', SetScheduleViewSet, base_name='user-')

'''
exp:
curl -X GET http://127.0.0.1:7000/api/user/schedules/ -H "Content-Type: application/json" -H 'Accept-Language:ja-JP' -H "Authorization:JWT (token)"
'''
urlpatterns = patterns('',
                       url(r'^api-token-auth/', obtain_jwt_token),
                       url(r'^user/schedules/$', user_schedules),
                       url(r'^user/schedules/(?P<user_id>\d+)/', include(user_router.urls)),
                       )

urlpatterns += router.urls
