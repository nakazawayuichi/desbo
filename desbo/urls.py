from django.conf.urls import url, include
from django.contrib import admin
from django.http import HttpResponseRedirect
from registration.backends.default.views import RegistrationView

from desbo import settings


def index(request):
    return HttpResponseRedirect(settings.INDEX_URL)

from django import forms
from django.contrib.auth.models import User   # fill in custom user info then save it
from django.contrib.auth.forms import UserCreationForm


class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField()
    first_name = forms.CharField(label='表示名') # Djano Userのfirst_nameを表示名とする

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email', 'password1', 'password2')

    def save(self,commit = True):
        user = super(MyRegistrationForm, self).save(commit = False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        if commit:
            user.save()
        return user

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('api.urls')),
    url(r'accounts/register/$', RegistrationView.as_view(form_class=MyRegistrationForm), name='registration_register'),
    url(r'^accounts/', include('registration.backends.default.urls')),
]
