import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import FlatButton from 'material-ui/FlatButton';

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';


var DestBoardParts = React.createClass({

  propTypes: {
    user_id: React.PropTypes.number,
    schedule_id: React.PropTypes.number,
    first_name: React.PropTypes.string,
    is_absence: React.PropTypes.bool,
    is_locked: React.PropTypes.bool,
    destination: React.PropTypes.string,
    attend_datetime: React.PropTypes.string,
    setSchedule: React.PropTypes.func.isRequired, // スケジュール設定
  },
  getInitialState() {
    return {
      destination: this.props.destination,
      attend_datetime: this.props.attend_datetime,
    };
  },
  onButtonClick(e) {
    if (!this.props.is_locked) { // ロックではない
      this.setState({ is_absence: ! this.state.is_absence });
      this.props.setSchedule(this.props.user_id, this.props.schedule_id, !this.props.is_absence, this.state.destination, this.state.attend_datetime);
    }
  },
  onTextChange(e) {
    this.setState({
      destination: e.target.value,
    });
  },
  onTextKeyDown(e) {
    if (e.key === 'Enter') {
      console.log(e.target.value);
      this.setSchedule();
    }
  },
  onDateChange(event, date) {
    this.setState({
      attend_datetime: date,
    });
    this.state.attend_datetime = date;
    this.setSchedule();
  },
  onTimeChange(event, time) {
    this.setState({
      attend_datetime: time,
    });
    this.state.attend_datetime = time;
    this.setSchedule();
  },
  onClearClick(){
    this.setState({
      attend_datetime: null
    });
    this.state.attend_datetime = null;
    this.setSchedule();
  },
  setSchedule() {
    this.props.setSchedule(
        this.props.user_id,
        this.props.schedule_id,
        this.props.is_absence,
        this.state.destination,
        this.state.attend_datetime
    );
  },
  render() {
    var attend_datetime;
    if ((this.state.attend_datetime != "") && (this.state.attend_datetime != null)) {
      attend_datetime = new Date(this.state.attend_datetime);
    } else {
      attend_datetime = null;
    }
    console.log(attend_datetime);

    return (
      <TableRow key={this.props.user_id}>
        <TableRowColumn>
          <RaisedButton label={ this.props.first_name }
                        secondary={ this.props.is_absence }
                        style ={{ "width":10 + "em" }}
                        disabled={ this.props.is_locked }
                        onClick={ this.onButtonClick }
          />
        </TableRowColumn>
        <TableRowColumn>
          <TextField hintText="行き先を入力"
                     value={ this.state.destination }
                     disabled={ this.props.is_locked }
                     onChange={ this.onTextChange }
                     onKeyDown={ this.onTextKeyDown }
          />
        </TableRowColumn>
        <TableRowColumn>
          <DatePicker hintText="帰社日を入力"
                      style ={{ "width":8 + "em" }}
                      value={ attend_datetime }
                      disabled={ this.props.is_locked }
                      onChange={this.onDateChange}
          />
        </TableRowColumn>
        <TableRowColumn>
          <TimePicker hintText="帰社時刻を入力"
                      format="24hr"
                      style ={{ "width":8 + "em" }}
                      value={ attend_datetime }
                      disabled={ this.props.is_locked }
                      onChange={this.onTimeChange}
          />
        </TableRowColumn>
        <TableRowColumn>
          <RaisedButton label="帰社日時クリア"
                        primary={true}
                        onClick={ this.onClearClick }
          />
        </TableRowColumn>
      </TableRow>
    );

  }
});

export default DestBoardParts;
