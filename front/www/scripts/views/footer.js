var React = require('react');

//フッターの定義
var Footer = React.createClass({
  render: function(){
    return (
      <footer style={{textAlign: "center"}}>
        <hr/>
        <h1>This in footer!!</h1>
      </footer>
    );
  }
});

module.exports = Footer;