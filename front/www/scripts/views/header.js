import React from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';

var ReactRouter = require('react-router');
var History = ReactRouter.History;

//コンポーネントを一つにまとめる
var Header = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },
  getInitialState() {
    return {};
  },
  handleTouchTap() {
    var result = confirm('ログアウトしますか？');
    if(result) {
      localStorage.setItem('token', ""); // トークンをリセット
      //ログイン画面へ
      this.context.router.push('/login');
    }
  },
  onClick() {
    var result = confirm('ログアウトしますか？');
    if(result) {
      localStorage.setItem('token', ""); // トークンをリセット
      //ログイン画面へ
      this.context.router.push('/login');
    }
  },
  render: function () {
    var date = new Date();
    const date_ja= new Array("日","月","火","水","木","金","土");

    var year = date.getFullYear();
    var month = date.getMonth()+1;
    var week = date.getDay();
    var day = date.getDate();

    var title = "行き先ボード - desbo  【 " + year+"年"+month+"月"+day+"日 "+date_ja[week]+"曜日 】";

    return (
        <AppBar
            title={title}
            iconElementRight={<FlatButton label="ログアウト" onClick={ this.onClick } />}
        />
    );
  }
})

export default Header;