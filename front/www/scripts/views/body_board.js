var request = require('superagent');
var ReactRouter = require('react-router');
var History = ReactRouter.History;

import React from 'react';
import DestBoardParts from './body_board_parts';
import Util from '../libs/utils'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

var DestBoardBody = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },
  propTypes: {
    userData: React.PropTypes.object ,
    updateSchedules: React.PropTypes.func.isRequired, // スケジュール更新
    token: React.PropTypes.string,
  },
  getInitialState() {
    return {
      is_absence: false,
      style: "{ margin:12 }",
    };
  },
  setSchedule:function(user_id, schedule_id, is_state, destination, attend_datetime) {

    var url;

    if (attend_datetime == "") {
      attend_datetime = null;
    }

    if (schedule_id == null) {
      url = Util.getRootURI() + "/api/user/schedules/" + user_id + "/set/";
      request
        .post(url)
        .set('Authorization', "JWT " + this.props.token)
        .send({is_attend:is_state, destination:destination, attend_datetime:attend_datetime })
        .withCredentials()
        .end(function(err, res){
          if (err) {
            if (res.status === 403) {
                //ログイン画面へ
                this.context.router.push('/login');
              }
          }
          this.props.updateSchedules();
         }.bind(this));
    } else {
      url = Util.getRootURI() + "/api/user/schedules/" + user_id + "/set/" + schedule_id + "/";
      request
        .put(url)
        .set('Authorization', "JWT " + this.props.token)
        .send({is_attend:is_state, destination:destination, attend_datetime:attend_datetime })
        .withCredentials()
        .end(function(err, res){
          if (err) {
            if (res.status === 403) {
              //ログイン画面へ
              this.context.router.push('/login');
            }
          }
          this.props.updateSchedules();
        }.bind(this));
    }

  },
  render:function(){
    return (
    <div>
      <table>
       <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
       >
         <TableRow>
          <TableHeaderColumn>氏名</TableHeaderColumn>
          <TableHeaderColumn>行き先</TableHeaderColumn>
          <TableHeaderColumn>帰社日</TableHeaderColumn>
          <TableHeaderColumn>帰社時刻</TableHeaderColumn>
         </TableRow>
       </TableHeader>
       <TableBody>
      {this.props.userData['user_schedules'].map(function(schedule, i) {
        console.log('user_id=%s display_name=%s is_absence:%s is_locked:%s attend_date:%s',
            schedule['id'], schedule['display_name'], schedule['is_attend'], schedule['is_locked'], schedule['attend_date']);

        return (
            <DestBoardParts
              key={schedule['id']}
              setSchedule={this.setSchedule}
              user_id={schedule['id']}
              schedule_id={schedule['schedule_id']}
              first_name={schedule['display_name']}
              is_absence={schedule['schedule_id']}
              is_absence={schedule['is_attend']}
              is_locked={schedule['is_locked']}
              destination={schedule['destination']}
              attend_datetime={schedule['attend_datetime']}
              />
        );
       }, this)}
        </TableBody>
       </table>
    </div>
   )
  }
});

export default DestBoardBody;
