var React = require('react');
var request = require('superagent');
var ReactRouter = require('react-router');
var History = ReactRouter.History;

import DestBoardBody from './body_board';
import Util from '../libs/utils'

//コンポーネントを一つにまとめる
var Body = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },
  getInitialState() {
    return {
      userData: {user_schedules:[]},
      token: localStorage.getItem("token"),
    };
  },
  getUserSchedules:function() {
    var url =  Util.getRootURI() + "/api/user/schedules/";
    //ajax通信する
    request
      .get(url)
      .query({})
      .set('Authorization', "JWT " + this.state.token)
      .end(function(err, res){
        if (err) {
          if (res.status === 403) {
            //ログイン画面へ
            this.context.router.push('/login');
          }
        }
        this.setState({userData: JSON.parse(res.text)});
      }.bind(this));
  },
  updateSchedules:function() {
    this.getUserSchedules();
  },
  componentDidMount:function(){
    if (this.state.token != "") { // token有り
      console.log("state token:%s", this.state.token);
      this.updateSchedules();
    } else {
      //ログイン画面へ
      this.context.router.push('/login');
    }
  },
  render:function(){
    return (
      <div>
        <DestBoardBody updateSchedules={this.updateSchedules} userData={this.state.userData} token={this.state.token} />
      </div>
    );
  }
});

module.exports = Body;
