var os = require("os");

class Util {
  static getRootURI() {
    "use strict";

    let hostname = os.hostname();

    if (hostname.match(/127.0.0.1/)) {
      hostname =  "http://" + hostname + ":7000";
    } else {
      hostname =  "https://desbo.herokuapp.com";
    }
    return hostname
  }
}

export default Util;