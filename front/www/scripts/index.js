var React = require('react');
var ReactDOM = require('react-dom');

var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var History = ReactRouter.History;
var hashHistory = ReactRouter.hashHistory;

import Header from './views/header';
import Body from './views/body';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Util from './libs/utils'
import injectTapEventPlugin from 'react-tap-event-plugin';


var request = require('superagent');
injectTapEventPlugin();

//コンポーネントを一つにまとめる
var Index = React.createClass({
  render: function () {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
});

var Login = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },
  handleSubmit:function(e){
    e.preventDefault();

    var username = ReactDOM.findDOMNode(this.refs.username).value.trim();
    var password = ReactDOM.findDOMNode(this.refs.password).value.trim();
    this.login(username, password);
  },
  getInitialState() {
    return {
      errmessage: "",
      regist_url:Util.getRootURI() + "/accounts/register/"
    };
  },
  login:function(username, password) {
    console.log("username:%s password:%s", username, password);

    var url = Util.getRootURI() + "/api/api-token-auth/";
    //ajax通信する
    request
      .post(url)
      .send({username:username, password:password })
      .end(function(err, res){
        if (err) {
          this.setState({errmessage:"ユーザ名、もしくはパスワードが誤っています。"});
          return;
        }
        var token = res.body.token;
        console.log("username:%s token:%s", username, token);
        localStorage.setItem('token', token);

        //ポータル画面へ
        this.context.router.push('/schedule');
      }.bind(this));
  },
  render:function(){
    return (
      <div>
        <div className="main">
          <h1>行き先ボード - ログイン</h1>
          <form onSubmit={this.handleSubmit}>
            <input placeholder="ユーザー名" ref="username"/><br />
            <input type="password" placeholder="パスワード" ref="password"/><br />
            <span style={{color:"#ff0000"}} ref="errmessage">{this.state.errmessage}</span>
            <div style={{textAlign:"left"}}>
              <button type="submit">ログイン</button>
            </div>
          </form>
          <br/>
          <a ref="user_register_url" href={this.state.regist_url}>ユーザ登録は、こちら</a>
        </div>
      </div>
    );
  }
});

var Main = React.createClass({
  render:function(){
    return (
      <MuiThemeProvider>
      <div>
        <Header />
        <div className="main">
          {this.props.children}
        </div>
      </div>
      </MuiThemeProvider>
    );
  }
});

var Routes = (
  <Route path="/" component={Index}>
    <IndexRoute component={Login}/> /* "/"指定は、Loginへ遷移 */
    <Route path="/login" component={Login}/>
    <Route path="/schedule" component={Main}>
      <IndexRoute component={Body}/>
    </Route>
  </Route>
);

ReactDOM.render(
  <Router history={hashHistory}>{Routes}</Router>,
  document.getElementById('content')
);
