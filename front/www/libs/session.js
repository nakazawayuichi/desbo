var login_session = {};

(function(_login_session) {
  _login_session.init = function() {
    var STORAGE_ITEM_TOKEN = "login_token"

    function set_token(token) {
      sessionStorage.setItem('STORAGE_ITEM_TOKEN', token);
    };

    function get_token() {
      return sessionStorage.getItem('STORAGE_ITEM_TOKEN', token);
    };

    function clear_token(token) {
      sessionStorage.removeItem('STORAGE_ITEM_TOKEN');
      sessionStorage.clear();
    };

  };
})(login_session);
